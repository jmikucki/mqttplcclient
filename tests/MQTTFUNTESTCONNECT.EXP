
(* @NESTEDCOMMENTS := 'Yes' *)
(* @PATH := '\/Tests' *)
(* @OBJECTFLAGS := '0, 8' *)
(* @SYMFILEFLAGS := '2048' *)
FUNCTION MqttFunTestConnect : BOOL
VAR_INPUT
	resultsPtr : POINTER TO MQTT_TEST_STATE;
	sessionPtr : POINTER TO MQTT_SESSION;
END_VAR
VAR
	tcp_connection_active : BOOL := FALSE;

	testVarIntPublish : INT := 0;
	testVarString : STRING(10);
	testVarDint : DINT := 0;
	testVarUdint : UDINT := 0;
	testVarInt : INT := 0;
	testVarUint : UINT := 0;
	testVarBool : BOOL := FALSE;

	testCompleted : BOOL := FALSE;

	sendBufferData : ARRAY[0..200] OF BYTE;
	receiveBufferData : ARRAY[0..200] OF BYTE;
	testPositionReq: UINT := 16#00;
	testVectorReq : ARRAY[0..200] OF BYTE;
	testPositionResp: UINT := 16#00;
	testVectorResp : ARRAY[0..200] OF BYTE;
END_VAR
(* @END_DECLARATION := '0' *)
(*
	Author: Jerzy Mikucki
	All rights reserved 2017
	Released under license: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License 
*)

(* First setup session data *)
MqttTestSessionSetDefault(sessionPtr, ADR(sendBufferData), 200, ADR(receiveBufferData), 200);
sessionPtr^.session_state := MQTT_SS_CONNECTED;
testPositionReq := 0;
testPositionResp := 0;

(* Next create a proper buffer structures *)
sessionPtr^.send_buffer.bufferPtr := ADR(sendBufferData);
sessionPtr^.send_buffer.bufferPosition := 0;
sessionPtr^.send_buffer.bufferSize := 2000;
sessionPtr^.receive_buffer.bufferPtr := ADR(receiveBufferData);
sessionPtr^.receive_buffer.bufferPosition := 0;
sessionPtr^.receive_buffer.bufferSize := 2000;

testPositionReq := 0;
testPositionResp := 0;

(* We start with disconnected state, and tcp connection inactive. Shouldn't send anything *)
MqttMainProcessing(sessionPtr, tcp_connection_active);
MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition = 0, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.receive_buffer.bufferPosition = 0, FALSE);

(* Jump some time and make sure we still don't send. *)
sessionPtr^.current_time := sessionPtr^.current_time + t#10ms;
MqttMainProcessing(sessionPtr, tcp_connection_active);

MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition = 0, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.receive_buffer.bufferPosition = 0, FALSE);

(* Change the state to connecting to initiate a connection *)
sessionPtr^.session_state := MQTT_SS_CONNECTING_START;
MqttMainProcessing(sessionPtr, tcp_connection_active);

(* There shouldn't be any data in receive buffer generated *)
MqttTestSetState(resultsPtr, sessionPtr^.receive_buffer.bufferPosition = 0, FALSE);

(* We got the tcp connection, let's check that the mqtt is trying to connect *)
tcp_connection_active := TRUE;
MqttMainProcessing(sessionPtr, tcp_connection_active);

testPositionReq := MqttTestByteToBuffer(ADR(testVectorReq), testPositionReq, 16#10); (* Message type, dup, qos, retain *)
testPositionReq := MqttTestByteToBuffer(ADR(testVectorReq), testPositionReq, 16#00); (* Remaining length, dummy *)
testPositionReq := MqttTestStrToBufferUTF8(ADR(testVectorReq), testPositionReq, 'MQIsdp');
testPositionReq := MqttTestByteToBuffer(ADR(testVectorReq), testPositionReq, 16#03); (* Version *)
testPositionReq := MqttTestByteToBuffer(ADR(testVectorReq), testPositionReq, 16#CE); (* Flags *)
testPositionReq := MqttTestByteToBuffer(ADR(testVectorReq), testPositionReq, 16#00);
testPositionReq := MqttTestByteToBuffer(ADR(testVectorReq), testPositionReq, 16#00); (* Keep Alive timer*);
testPositionReq := MqttTestStrToBufferUTF8(ADR(testVectorReq), testPositionReq, 'testIdentifier');
testPositionReq := MqttTestStrToBufferUTF8(ADR(testVectorReq), testPositionReq, 'testTopic');
testPositionReq := MqttTestStrToBufferUTF8(ADR(testVectorReq), testPositionReq, 'testWill');
testPositionReq := MqttTestStrToBufferUTF8(ADR(testVectorReq), testPositionReq, 'testUsername');
testPositionReq := MqttTestStrToBufferUTF8(ADR(testVectorReq), testPositionReq, 'testPassword');
MqttTestByteToBuffer(ADR(testVectorReq), 1, UINT_TO_BYTE(testPositionReq-2)); (* Remaining length, real *)

(* Now we would expect data on the send buffer. *)
MqttTestSetState(resultsPtr, MqttTestCompareBuffers(sessionPtr^.send_buffer.bufferPtr, sessionPtr^.send_buffer.bufferPosition, ADR(testVectorReq), testPositionReq) = TRUE, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.receive_buffer.bufferPosition = 0, FALSE);

(* Consume all data *)
sessionPtr^.send_buffer.bufferPosition := MqttTestConsumeBuffer(sessionPtr^.send_buffer.bufferPtr, sessionPtr^.send_buffer.bufferPosition, sessionPtr^.send_buffer.bufferPosition);

(* Prepare response from the server *)
testPositionResp := MqttTestByteToBuffer(ADR(testVectorResp), testPositionResp, 16#20); (* Message type, dup, qos, retain *)
testPositionResp := MqttTestByteToBuffer(ADR(testVectorResp), testPositionResp, 16#02); (* Remaining length, real *)
testPositionResp := MqttTestByteToBuffer(ADR(testVectorResp), testPositionResp, 16#00); (* Topic name compression, reserved *)
testPositionResp := MqttTestByteToBuffer(ADR(testVectorResp), testPositionResp, 16#00); (* Return code, accepted *)

sessionPtr^.receive_buffer.bufferPosition := MqttTestCopyBuffer(ADR(testVectorResp), testPositionResp, sessionPtr^.receive_buffer.bufferPtr, sessionPtr^.receive_buffer.bufferPosition);
MqttMainProcessing(sessionPtr, tcp_connection_active);

(* Now that we got response, we should be in connected state *)
MqttTestSetState(resultsPtr, sessionPtr^.session_state = MQTT_SS_CONNECTED, FALSE);

(* The test was completed with some results *)
MqttTestSetState(resultsPtr, TRUE, TRUE);
END_FUNCTION
