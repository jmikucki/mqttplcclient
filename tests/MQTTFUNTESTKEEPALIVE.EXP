
(* @NESTEDCOMMENTS := 'Yes' *)
(* @PATH := '\/Tests' *)
(* @OBJECTFLAGS := '0, 8' *)
(* @SYMFILEFLAGS := '2048' *)
FUNCTION MqttFunTestKeepalive : BOOL
VAR_INPUT
	resultsPtr : POINTER TO MQTT_TEST_STATE;
	sessionPtr : POINTER TO MQTT_SESSION;
END_VAR
VAR
	tcp_connection_active : BOOL := FALSE;

	testVarIntPublish : INT := 0;
	testVarString : STRING(10);
	testVarDint : DINT := 0;
	testVarUdint : UDINT := 0;
	testVarInt : INT := 0;
	testVarUint : UINT := 0;
	testVarBool : BOOL := FALSE;

	results : MQTT_TEST_STATE;
	testCompleted : BOOL := FALSE;

	sendBufferData : ARRAY[0..200] OF BYTE;
	receiveBufferData : ARRAY[0..200] OF BYTE;
	testPositionReq: UINT := 16#00;
	testVectorReq : ARRAY[0..200] OF BYTE;
	testPositionResp: UINT := 16#00;
	testVectorResp : ARRAY[0..200] OF BYTE;
END_VAR
(* @END_DECLARATION := '0' *)
(*
	Author: Jerzy Mikucki
	All rights reserved 2017
	Released under license: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License 
*)

(* First setup session data *)
MqttTestSessionSetDefault(sessionPtr, ADR(sendBufferData), 200, ADR(receiveBufferData), 200);
sessionPtr^.session_state := MQTT_SS_CONNECTED;
sessionPtr^.broker_keepalive := t#5s;
sessionPtr^.qos_retry_time := t#3s;
sessionPtr^.ping_retry_max := 3;
testPositionReq := 0;
testPositionResp := 0;

(* Next create a proper buffer structures *)
sessionPtr^.send_buffer.bufferPtr := ADR(sendBufferData);
sessionPtr^.send_buffer.bufferPosition := 0;
sessionPtr^.send_buffer.bufferSize := 2000;
sessionPtr^.receive_buffer.bufferPtr := ADR(receiveBufferData);
sessionPtr^.receive_buffer.bufferPosition := 0;
sessionPtr^.receive_buffer.bufferSize := 2000;

testPositionReq := 0;
testPositionResp := 0;

(* We start with connected state and zero wall time *)
MqttMainProcessing(sessionPtr, tcp_connection_active);

(* Send buffer should be empty *)
MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition = 0, FALSE);

(* Jump some time and make sure we still don't send. *)
sessionPtr^.current_time := sessionPtr^.current_time + t#4s;
MqttMainProcessing(sessionPtr, tcp_connection_active);
MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition = 0, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.session_state = MQTT_SS_CONNECTED, FALSE);

(* Jump some more time and make sure we send 1st. *)
sessionPtr^.current_time := sessionPtr^.current_time + t#2s;
MqttMainProcessing(sessionPtr, tcp_connection_active);
MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition <> 0, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.session_state = MQTT_SS_CONNECTED, FALSE);
(* Consume all data *)
sessionPtr^.send_buffer.bufferPosition := MqttTestConsumeBuffer(sessionPtr^.send_buffer.bufferPtr, sessionPtr^.send_buffer.bufferPosition, sessionPtr^.send_buffer.bufferPosition);

(* Jump some more time and make sure we send retry 2nd. *)
sessionPtr^.current_time := sessionPtr^.current_time + sessionPtr^.qos_retry_time + t#1s;
MqttMainProcessing(sessionPtr, tcp_connection_active);
MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition <> 0, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.session_state = MQTT_SS_CONNECTED, FALSE);
(* Consume all data *)
sessionPtr^.send_buffer.bufferPosition := MqttTestConsumeBuffer(sessionPtr^.send_buffer.bufferPtr, sessionPtr^.send_buffer.bufferPosition, sessionPtr^.send_buffer.bufferPosition);

(* Jump some more time and make sure we send retry 3rd. *)
sessionPtr^.current_time := sessionPtr^.current_time + sessionPtr^.qos_retry_time + t#1s;
MqttMainProcessing(sessionPtr, tcp_connection_active);
MqttTestSetState(resultsPtr, sessionPtr^.send_buffer.bufferPosition <> 0, FALSE);
MqttTestSetState(resultsPtr, sessionPtr^.session_state = MQTT_SS_CONNECTED, FALSE);
(* Consume all data *)
sessionPtr^.send_buffer.bufferPosition := MqttTestConsumeBuffer(sessionPtr^.send_buffer.bufferPtr, sessionPtr^.send_buffer.bufferPosition, sessionPtr^.send_buffer.bufferPosition);

(* Jump some more time and make sure we are disconnected. *)
sessionPtr^.current_time := sessionPtr^.current_time + sessionPtr^.qos_retry_time + t#1s;
MqttMainProcessing(sessionPtr, tcp_connection_active);
MqttTestSetState(resultsPtr, sessionPtr^.session_state <> MQTT_SS_CONNECTED, FALSE);
(* Consume all data *)
sessionPtr^.send_buffer.bufferPosition := MqttTestConsumeBuffer(sessionPtr^.send_buffer.bufferPtr, sessionPtr^.send_buffer.bufferPosition, sessionPtr^.send_buffer.bufferPosition);

(* The test was completed with some results *)
MqttTestSetState(resultsPtr, TRUE, TRUE);
END_FUNCTION
