
(* @NESTEDCOMMENTS := 'Yes' *)
(* @PATH := '\/Internal\/PrepareHeader' *)
(* @OBJECTFLAGS := '0, 8' *)
(* @SYMFILEFLAGS := '2048' *)
FUNCTION MqttPrepareFixedHeader : UINT
VAR_INPUT
	bufferPtr : POINTER TO ARRAY[0..32767] OF BYTE;
	size : UINT;
	possition : UINT;
	message_type : MQTT_MESSAGE_TYPE;
	message_length_remaining : UINT; (* Total length including fixed header *)
	dup_flag : BYTE;
	qos_level: BYTE;
	retain_flag: BYTE;
END_VAR
VAR
	current_possition : UINT;
	current_byte: BYTE;
	message_type_byte: BYTE;
	error : INT := 0;
END_VAR
VAR CONSTANT
	message_type_shift : UINT := 4;
	dup_flag_shift: UINT := 3;
	qos_level_shift: UINT := 1;
END_VAR
(* @END_DECLARATION := '0' *)
(*
	Author: Jerzy Mikucki
	All rights reserved 2017
	Released under license: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License 
*)

(* Setup the parameters of buffer *)
current_possition := possition;
message_type_byte := MqttMessageTypeToByte(message_type);

(* Prepare first byte of fixed header. *)
current_byte := SHL(message_type_byte, message_type_shift) OR SHL(dup_flag,  dup_flag_shift) OR SHL(qos_level,  qos_level_shift) OR retain_flag;
bufferPtr^[current_possition] := current_byte;
current_possition := current_possition + 1;

(* Prepare a second byte of fixed header *)
IF (message_length_remaining <= 128) THEN
	bufferPtr^[current_possition] := UINT_TO_BYTE(message_length_remaining);
	current_possition := current_possition + 1;
ELSIF (message_length_remaining <= 16384) THEN
	bufferPtr^[current_possition] := UINT_TO_BYTE( (message_length_remaining MOD 128) + 128);
	current_possition := current_possition + 1;
	bufferPtr^[current_possition] := UINT_TO_BYTE(( (message_length_remaining - (message_length_remaining MOD 128)) /128)+ 1);
	current_possition := current_possition + 1;
ELSE
	(* Unsupported lenght of message *)
	MqttPrepareFixedHeader := 0;
	RETURN;
END_IF;


MqttPrepareFixedHeader := current_possition - possition;
RETURN;
END_FUNCTION
