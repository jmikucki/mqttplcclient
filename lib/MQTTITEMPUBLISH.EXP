
(* @NESTEDCOMMENTS := 'Yes' *)
(* @PATH := '' *)
(* @OBJECTFLAGS := '0, 8' *)
(* @SYMFILEFLAGS := '2048' *)
FUNCTION MqttItemPublish : INT
VAR_INPUT
	session : POINTER TO MQTT_SESSION;
	topic : STRING;
	bufferValuePtr : POINTER TO ARRAY[0..32767] OF BYTE;
	bufferValueSize : UINT;
	bufferValueType : MQTT_SINGLE_ITEM_TYPE;
	qos : MQTT_QOS_TYPE;
END_VAR
VAR
	item_nr : INT := 0;
	itemPtr : POINTER TO MQTT_SINGLE_ITEM;
	value_endian: BOOL;
END_VAR
(* @END_DECLARATION := '0' *)
(*
	Author: Jerzy Mikucki
	All rights reserved 2017
	Released under license: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License 
*)

item_nr := MqttGetItemNrForTopic(topic, MQTT_PUBLISH_DIRECTION, session);
IF item_nr = -1 THEN
	item_nr := MqttGetItemNrFirstEmpty(session);
END_IF;

IF item_nr = -1 OR item_nr = session^.items_max + 1 THEN
	MqttItemPublish := -1;
	RETURN;
END_IF;

IF item_nr >= session^.items_first_notregistered_idx THEN
	session^.items_first_notregistered_idx := item_nr + 1;
END_IF;

itemPtr := ADR(session^.items[item_nr]);

itemPtr^.topic := topic;
itemPtr^.value_ptr := bufferValuePtr;
itemPtr^.value_size := bufferValueSize;
itemPtr^.value_type  := bufferValueType;
IF bufferValueType = MQTT_SIT_4_BYTEARRAY THEN
	itemPtr^.value_endian := 4;
END_IF;
itemPtr^.direction := MQTT_PUBLISH_DIRECTION;
itemPtr^.interval  := T#0s;
itemPtr^.last_txrx_time := DT#1970-01-01-00:00;
itemPtr^.last_success_time := DT#1970-01-01-00:00;
itemPtr^.remove_after := FALSE;
itemPtr^.state := MQTT_SIS_PUBLISHED;
itemPtr^.trigger := TRUE;
itemPtr^.remove_after := TRUE;

itemPtr^.qos_level := qos;
itemPtr^.retry_attempt_nr := 0;

(* Return success *)
MqttItemPublish := 0;
END_FUNCTION
