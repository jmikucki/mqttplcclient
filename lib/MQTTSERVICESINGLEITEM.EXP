
(* @NESTEDCOMMENTS := 'Yes' *)
(* @PATH := '\/Internal' *)
(* @OBJECTFLAGS := '0, 8' *)
(* @SYMFILEFLAGS := '2048' *)
FUNCTION MqttServiceSingleItem : INT
VAR_INPUT
	item : POINTER TO MQTT_SINGLE_ITEM;
	send_buffer : POINTER TO ARRAY[0..32767] OF BYTE;
	send_size : POINTER TO UINT;
	session_ptr : POINTER TO MQTT_SESSION;
END_VAR
VAR
	current_state : MQTT_SINGLE_ITEM_STATE := MQTT_SIS_STOPPED;
	error : INT := 0;
END_VAR
(* @END_DECLARATION := '0' *)
(*
	Author: Jerzy Mikucki
	All rights reserved 2017
	Released under license: Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International Public License 
*)

current_state := item^.state;

CASE current_state OF

MQTT_SIS_STOPPED:
	send_size^ := 0;
	MqttServiceSingleItem := 1;  (* Success go to next item *)

MQTT_SIS_PUBLISHED:
	(* Send a publish message when:
		- item is a triggered publis and it was just triggered
		- last time we send this item was more than interval ago, and no retry is in progress
		- we didn't get a response on qos message, so retry
	*)
	IF item^.interval = T#0s THEN
		IF item^.trigger = TRUE THEN
			error := MqttPrepareSinglePublishItem(item, send_buffer, send_size, session_ptr);
			item^.last_txrx_time := session_ptr^.current_time;
			item^.trigger := FALSE;
			CASE item^.qos_level OF
			0:
				item^.last_success_time := session_ptr^.current_time;
				item^.retry_attempt_nr := 0;
			1:
				item^.retry_attempt_nr := 1;
			2:
				;
			END_CASE;
		ELSE
			send_size^ := 0;
		END_IF;
	ELSIF (item^.retry_attempt_nr = 0) AND (item^.last_success_time + item^.interval <= session_ptr^.current_time) THEN
		error := MqttPrepareSinglePublishItem(item, send_buffer, send_size, session_ptr);
		item^.last_txrx_time := session_ptr^.current_time;
		CASE item^.qos_level OF
		0:
			item^.last_success_time := session_ptr^.current_time;
			item^.retry_attempt_nr := 0;
		1:
			item^.retry_attempt_nr := 1;
		2:
			;
		END_CASE;
	ELSIF item^.retry_attempt_nr > 0 AND
		    item^.last_txrx_time + session_ptr^.qos_retry_time <= session_ptr^.current_time THEN
		item^.last_txrx_time := session_ptr^.current_time;
		item^.retry_attempt_nr := item^.retry_attempt_nr + 1;
		error := MqttPrepareSinglePublishItem(item, send_buffer, send_size, session_ptr);
	ELSE
		send_size^ := 0;
	END_IF;

	MqttServiceSingleItem := 1; (* Don't get back to this item*)

MQTT_SIS_SUBSCRIBED_INIT:
	error := MqttPrepareSingleSubscribeItem(item, send_buffer, send_size, session_ptr);
	MqttServiceSingleItem := 1;  (* Success go to next item *)

MQTT_SIS_SUBSCRIBED_WAIT_FOR_ACK:
	(* We don't do anything here. We need to wait for a received ack*)
	send_size^ := 0;
	error := 0;
	MqttServiceSingleItem := 1;  (* Success go to next item *)


MQTT_SIS_SUBSCRIBED_SEND_ACK:
	send_size^ := MqttPreparePubackPacket(item, send_buffer, send_size^, session_ptr);
	item^.state := MQTT_SIS_SUBSCRIBED;
	MqttServiceSingleItem := 1;  (* Success go to next item *)

MQTT_SIS_SUBSCRIBED_UNSUB_INIT:
	send_size^ := MqttPrepareUnsubscribePacket(item, send_buffer, send_size^, session_ptr);
	item^.state := MQTT_SIS_SUBSCRIBED_WAIT_FOR_UNSUBACK;
	MqttServiceSingleItem := 1;  (* Success go to next item *)

MQTT_SIS_SUBSCRIBED_WAIT_FOR_UNSUBACK:
	(* We don't do anything here. We need to wait for a received ack*)
	send_size^ := 0;
	error := 0;
	MqttServiceSingleItem := 1;  (* Success go to next item *)

MQTT_SIS_SUBSCRIBED:
	send_size^ := 0;
	error := 0;
	MqttServiceSingleItem := 1;  (* Success go to next item *)

END_CASE;

IF error < 0 THEN
	item^.state := MQTT_SIS_STOPPED;
	MqttServiceSingleItem := error;
END_IF;
END_FUNCTION
